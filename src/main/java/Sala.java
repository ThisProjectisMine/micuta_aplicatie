import java.util.*;

public class Sala implements Subject {


    private List<Observer> observers = new ArrayList<>();
    private List<Object> trackerSubscriberi = new ArrayList<>(); //
    private List<Object> locuriOcupateSala = new ArrayList<>(10);
    private Queue<Object> coadaDeIntrare = new LinkedList<>();


    public Queue<Object> getCoadaDeIntrare() {
        return coadaDeIntrare;
    }

    private String news;

    public List<Object> getLocuriOcupateSala() {
        return locuriOcupateSala;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);


    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public List<Observer> getObservers() {
        return observers;
    }

    @Override
    public void notifyObserver() {
        observers.forEach(e-> e.update(news));

    }

    public void setNews(String news) {
        this.news = news;
    }

    public List<Object> getTrackerSubscriberi() {
        return trackerSubscriberi;
    }
}
