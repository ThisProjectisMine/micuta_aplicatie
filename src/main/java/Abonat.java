import java.io.*;
import java.util.Date;
import java.util.List;

public class Abonat implements Observer,Comparable{

    private String email;
    private String name;
    private List<Date> intrari;
    private static int nr = 1;
    private int ID;
    private int progress;
    private String parola;
    private String antrenor;
    private Antrenor personalTrainer;

    public Abonat(String email, String name) {
        this.email = email;
        this.name = name;
        this.ID = getNr();
        setNr(nr);
        this.progress = progress;
        this.parola = null;
        this.antrenor = null;
        this.personalTrainer=null;
    }





    public Antrenor getPersonalTrainer() {
        return personalTrainer;
    }

    public void setPersonalTrainer(Antrenor personalTrainer) {
        this.personalTrainer = personalTrainer;
    }

    public void setAntrenor(String antrenor) {
        this.antrenor = antrenor;
    }

    public String getAntrenor() {
        return antrenor;
    }


    public String getEmail() {
        return email;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public void setProgress(int progress) {
        this.progress = progress;

    }

    public int getProgress() {
        return progress;
    }

    public static void setNr(int nr) {
        Abonat.nr = nr + 1;
    }

    public int getID() {
        return this.ID;
    }

    public static int getNr() {
        return nr;
    }

    @Override
    public String toString(){
        return this.email ;
    }


    public String getName() {
        return name;
    }

    @Override
    public void subscribe(Subject subject) {
        subject.addObserver(this);

    }

    @Override
    public void unsubscribe(Subject subject) {
        subject.removeObserver(this);
    }

    @Override
    public void update(String news) {
//        System.out.println("A fost notificata persoana cu adresa de email" + getEmail() + ", stirea cu mesajul " + news);
    }

    @Override

    public boolean equals(Object o){
        if(o==null || !(o instanceof Abonat)){
            return false;
        }
        Abonat k = (Abonat) o;
        if(this.getEmail().equals(k.getEmail()) && this.getName().equals(k.getName())){
            return true;
        }
        return false;
    }

    @Override

    public int hashCode(){
        int result = 17;

        result +=31*this.getEmail().hashCode();
        result +=31*this.getName().hashCode();

                return result;
    }


    @Override
    public int compareTo(Object o) {
        Abonat abonat = (Abonat) o;
      if(this.getProgress()>abonat.getProgress()){
          return -1;
      }else if(this.getProgress()==abonat.getProgress()){
          int compar =this.getEmail().compareTo(abonat.getEmail());
                  if(compar>0){
                      return -1;
                  }else{
                      return 1;
                  }
      }else return 1;


    };
}
