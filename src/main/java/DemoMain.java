import javax.swing.plaf.nimbus.State;
import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


public class DemoMain {
    public static void main(String[] args) {


        List<Abonat> listaAbonati = new ArrayList<>();
        List<Antrenor> listaAntrenor = new ArrayList<>();
        Sala worldClass = new Sala();


        int counter = 0;
        int counter1 = 0;
        boolean duplicat = false;
        boolean contExistent;
        boolean contExistent1;
        Abonat abonatLogatCurent = null;
        Antrenor antrenorLogatCurent = null;
        int capacitate = 10;


//        Rand rand = new Rand() {
//
//
//            @Override
//            public void run() {
//                Object obiectAdaugare = null;
//
//                while (!worldClass.getCoadaDeIntrare().isEmpty()) {
//
//                    if (worldClass.getLocuriOcupateSala().size() < capacitate) {
//
//                        obiectAdaugare = worldClass.getCoadaDeIntrare().peek();
//
//                        worldClass.getLocuriOcupateSala().add(obiectAdaugare);
//                        System.out.println("Am adaugat in sala persoana din coada" + obiectAdaugare);
//                        worldClass.getCoadaDeIntrare().poll();
//
//                    }
//                }
//            }
//        };


        try (FileReader fileReader = new FileReader("src/input.txt");
             FileWriter fileWriter = new FileWriter("src/output.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        ) {

            while (bufferedReader.read() != -1) {
                String text = bufferedReader.readLine();
                String[] comanda = text.split("\\s+");
                duplicat = false;

                contExistent = false;
                contExistent1 = false;

                Abonat indice = null;
                Antrenor indice1 = null;

                switch (comanda[0]) {

                    case "SIGNUP_ABONAT":

                        String verifStructura[] = comanda[1].split("@");


                        if (emailVerification(counter1, bufferedWriter, verifStructura) && uniqueMail(listaAbonati, listaAntrenor, duplicat, bufferedWriter, comanda) && passwordVerification(bufferedWriter, comanda)) {
                            Abonat abonat = new Abonat(comanda[1], comanda[2]);
                            abonat.setParola(comanda[5]);
                            Integer progres = Integer.parseInt(comanda[3]);
                            abonat.setProgress(progres);
                            listaAbonati.add(abonat);
                        }

                        break;

                    case "SIGNUP_ANTRENOR":

                        String verifStructura1[] = comanda[1].split("@");


                        if (emailVerification(counter1, bufferedWriter, verifStructura1) && uniqueMail(listaAbonati, listaAntrenor, duplicat, bufferedWriter, comanda) && passwordVerification(bufferedWriter, comanda)) {
                            Antrenor antrenor = new Antrenor(comanda[1], comanda[2]);
                            antrenor.setParola(comanda[5]);
                            Integer numarulMaximDeCursanti = Integer.parseInt(comanda[3]);
                            antrenor.setNumarMaxCursanti(numarulMaximDeCursanti);
                            listaAntrenor.add(antrenor);
                        }

                        break;

                    case "LOGIN_ABONAT":


                        for (Abonat i : listaAbonati) {
                            if ((i.getEmail().equals(comanda[1]))) {
                                contExistent = true;
                                System.out.println("Abonatul exista " + i.getEmail() + "\n");
                                indice = i;
                            }

                        }

                        if (contExistent == true) {
                            System.out.println(indice.getParola() + " comparam cu parola " + comanda[2]);
                            boolean match = indice.getParola().equals(comanda[2]);
                            if (match == false) {
                                bufferedWriter.write("Parola este incorecta " + comanda[2] + "\n");

                            } else if (abonatLogatCurent == null) {
                                abonatLogatCurent = indice;
                                System.out.println("Abonatul " + abonatLogatCurent.getEmail() + " s-a logat cu succes\n");

                            } else {
                                bufferedWriter.write("Exista deja un abonat connectat\n");
                            }
                        }
                        if (contExistent == false) {
                            bufferedWriter.write("Abonatul " + comanda[1] + " nu exista\n");
                            bufferedWriter.flush();
                        }


                        break;

                    case "LOGIN_ANTRENOR":

                        for (Antrenor i : listaAntrenor) {
                            if ((i.getEmail().equals(comanda[1]))) {
                                contExistent1 = true;
                                System.out.println("Antrenorul exista " + i.getEmail() + "\n");
                                indice1 = i;
                            }

                        }

                        if (contExistent1 == true) {
                            System.out.println(indice1.getParola() + " comparam cu parola " + comanda[2]);
                            boolean match = indice1.getParola().equals(comanda[2]);
                            if (match == false) {
                                bufferedWriter.write("Parola este incorecta " + comanda[2] + "\n");

                            } else if (antrenorLogatCurent == null) {
                                antrenorLogatCurent = indice1;
                                System.out.println("Abonatul " + antrenorLogatCurent.getEmail() + " s-a logat cu succes\n");

                            } else {
                                bufferedWriter.write("Exista deja un antrenor connectat\n");
                            }
                        }
                        if (contExistent1 == false) {
                            bufferedWriter.write("Abonatul " + comanda[1] + " nu exista\n");
                            bufferedWriter.flush();
                        }

                        break;


                    case "LOGOUT_ABONAT":


                        if (!comanda[1].equals(abonatLogatCurent.getEmail())) {
                            bufferedWriter.write("Abonatul nu este cel curent\n");
                            bufferedWriter.flush();
                        } else {
                            abonatLogatCurent = null;
                            System.out.println("Am delogat ");
                        }

                        break;

                    case "LOGOUT_ANTRENOR":

                        if (!comanda[1].equals(antrenorLogatCurent.getEmail())) {
                            bufferedWriter.write("Antrenorul nu este cel curent\n");
                            bufferedWriter.flush();
                        } else {
                            antrenorLogatCurent = null;
                            System.out.println("Am delogat ");
                        }
                        break;

                    case "INCREMENT_PROGRESS":
                        if (abonatLogatCurent == null) {
                            bufferedWriter.write("Nu este nici un abonat logat!");
                            bufferedWriter.flush();
                        } else {
                            Integer vloare = Integer.parseInt(comanda[1]);
                            Integer increment = abonatLogatCurent.getProgress() + vloare;
                            if (increment <= 10) {
                                abonatLogatCurent.setProgress(increment);
                                System.out.println("Progresul current al " + abonatLogatCurent.getEmail() + " este " + abonatLogatCurent.getProgress() + "\n");
                            } else {
                                bufferedWriter.write("Nu se poate face incrementarea, progresul total ar fi" + increment);
                                bufferedWriter.flush();
                            }

                        }
                        break;

                    case "DECREMENT_PROGRESS":
                        if (abonatLogatCurent == null) {
                            bufferedWriter.write("Nu este nici un abonat logat!\n");
                            bufferedWriter.flush();
                        } else {
                            Integer vloare = Integer.parseInt(comanda[1]);
                            Integer decrement = abonatLogatCurent.getProgress() - vloare;
                            if (decrement >= 0) {
                                abonatLogatCurent.setProgress(decrement);
                                System.out.println("progresul current al " + abonatLogatCurent.getEmail() + " este " + abonatLogatCurent.getProgress() + "\n");
                            } else {
                                bufferedWriter.write("Nu se poate face decrementarea, progresul total ar fi " + decrement + "\n");
                                bufferedWriter.flush();
                            }

                        }
                        break;

                    case "ADAUGA_ANTRENOR":

                        Antrenor provizoriu = null;

                        if (abonatLogatCurent == null) {
                            bufferedWriter.write("Nu este nici un abonat logat!\n");
                            bufferedWriter.flush();
                        }
                        for (Antrenor i : listaAntrenor) {
                            if (i.getEmail().equals(comanda[1])) {

                                provizoriu = i;

                            }
                        }

                        if (provizoriu == null) {
                            bufferedWriter.write("Nu exista antrenorul cu emailul " + comanda[1] + "\n");
                            bufferedWriter.flush();
                        } else if (provizoriu.getListaDeCursanti().size() < provizoriu.getNumarMaxCursanti()) {
                            abonatLogatCurent.setPersonalTrainer(provizoriu);
                            provizoriu.getListaDeCursanti().add(abonatLogatCurent);

                        } else {
                            bufferedWriter.write("Antrenorul nu mai are locuri libere!\n");
                            bufferedWriter.flush();
                        }
                        break;

                    case "VIZUALIZARE_ABONATII_MEI":


                        if (antrenorLogatCurent == null) {
                            bufferedWriter.write("Nu există nici un antrenor logat!\n");
                            bufferedWriter.flush();
                        } else {

                            antrenorLogatCurent.getListaDeCursanti().forEach(e -> System.out.println("Abonatii cursului meu sunt " + e + "\n"));
                        }

                        break;

                    case "SUBSCRIBE_ABONAT":
                        Abonat beta;
                        Boolean existaObserver = false;
                        if (abonatLogatCurent == null) {
                            bufferedWriter.write("Nu exista nici un abonat logat \n");
                            bufferedWriter.flush();
                            break;
                        } else {
                            for (Object i : worldClass.getTrackerSubscriberi()) {
                                if (i instanceof Abonat) {
                                    beta = (Abonat) i;
                                    if (beta.equals(abonatLogatCurent)) {
                                        existaObserver = true;
                                    }
                                }
                            }
                            if (existaObserver == false) {
                                worldClass.getTrackerSubscriberi().add(abonatLogatCurent);
                                abonatLogatCurent.subscribe(worldClass);
                                bufferedWriter.write("Abonatul cu adresa de email " + abonatLogatCurent.getEmail() + " a fost abonat la newsletter\n");
                            }
                        }


                        break;

                    case "SUBSCRIBE_ANTRENOR":
                        Antrenor alpha;
                        Boolean existaObserverulAntrenor = false;
                        if (antrenorLogatCurent == null) {
                            bufferedWriter.write("Nu exista nici un Antrenor logat \n");
                            bufferedWriter.flush();
                            break;
                        } else {
                            for (Object i : worldClass.getTrackerSubscriberi()) {
                                if (i instanceof Antrenor) {
                                    alpha = (Antrenor) i;
                                    if (alpha.equals(antrenorLogatCurent)) {
                                        existaObserverulAntrenor = true;
                                    }
                                }
                            }
                            if (existaObserverulAntrenor == false) {
                                worldClass.getTrackerSubscriberi().add(antrenorLogatCurent);
                                antrenorLogatCurent.subscribe(worldClass);
                                bufferedWriter.write("Antrenorul cu adresa de email " + antrenorLogatCurent.getEmail() + " a fost abonat la newsletter\n");
                            }
                        }
                        break;

                    case "ADAUGA_NEWS":

                        worldClass.setNews(comanda[1]);
                        worldClass.notifyObserver();
                        worldClass.getObservers().forEach(e -> {
                            if (e instanceof Abonat) {

                                Abonat abonatNotificat = (Abonat) e;
                                try {
                                    bufferedWriter.write("A fost notificata persoana cu adresa de email " + abonatNotificat.getEmail() + ", stirea cu mesajul " + comanda[1] + "\n");
                                    bufferedWriter.flush();
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                            } else if (e instanceof Antrenor) {
                                Antrenor antrenorNotificat = (Antrenor) e;
                                try {
                                    bufferedWriter.write("A fost notificata persoana cu adresa de email " + antrenorNotificat.getEmail() + ", stirea cu mesajul " + comanda[1] + "\n");
                                    bufferedWriter.flush();
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                            }


                        });
                        break;


                    case "INTRA_IN_SALA":

                        Abonat cuContCreat = null;
                        Antrenor cuContCreat1 = null;
                        boolean dejaInauntru = false;
                        Object adaugare = null;
                        LocalDateTime momentulIntrarii = LocalDateTime.now();

                        for (Abonat i : listaAbonati) {

                            if (i.getEmail().equals(comanda[1])) {

                                cuContCreat = i;
                            }
                        }

                        for (Antrenor i : listaAntrenor) {
                            if (i.getEmail().equals(comanda[1])) {
                                cuContCreat1 = i;
                            }
                        }


                        if (cuContCreat != null) {
                            for (Object o : worldClass.getLocuriOcupateSala()) {
                                if (o instanceof Abonat) {
                                    Abonat abonat = (Abonat) o;
                                    if (abonat.equals(cuContCreat)) {
                                        bufferedWriter.write("Persoana cu adresa de email " + cuContCreat.getEmail() + " este deja in sala\n");
                                        bufferedWriter.flush();
                                        dejaInauntru = true;
                                    }
                                }
                            }
                        }
                        if (dejaInauntru == false && cuContCreat != null) {
                            boolean avemInCoada = false;
                            if (!worldClass.getCoadaDeIntrare().isEmpty() && worldClass.getLocuriOcupateSala().size() < capacitate) {

                                adaugare = worldClass.getCoadaDeIntrare().peek();
                                worldClass.getLocuriOcupateSala().add(adaugare);
                                worldClass.getCoadaDeIntrare().poll();
                                System.out.println("Am adaugat abonatul " + cuContCreat.getEmail() + " din coada de intrare la data si ora " + momentulIntrarii);
                            } else if (worldClass.getLocuriOcupateSala().size() < capacitate) {
                                worldClass.getLocuriOcupateSala().add(cuContCreat);
                                System.out.println(cuContCreat.getEmail() + " a intrat in sala la data si ora " + momentulIntrarii);

                            } else {
                                for (Object o : worldClass.getCoadaDeIntrare()) {
                                    if (o instanceof Abonat) {
                                        Abonat incaUnu = (Abonat) o;
                                        if (incaUnu.equals(cuContCreat)) {
                                            avemInCoada = true;
                                        }
                                    }
                                }
                                if (avemInCoada == false) {
                                    System.out.println("Am adaugat in coada");
                                    worldClass.getCoadaDeIntrare().add(cuContCreat);
                                }
                            }
                        }

                        if (cuContCreat1 != null) {
                            for (Object o : worldClass.getLocuriOcupateSala()) {
                                if (o instanceof Antrenor) {
                                    Antrenor antrenorul = (Antrenor) o;
                                    if (antrenorul.equals(cuContCreat1)) {
                                        bufferedWriter.write("Persoana cu adresa de email " + cuContCreat1.getEmail() + " este deja in sala\n");
                                        bufferedWriter.flush();
                                        dejaInauntru = true;
                                    }
                                }
                            }
                        }
                        if (dejaInauntru == false && cuContCreat1 != null) {
                            boolean avemInCoada = false;
                            if (!worldClass.getCoadaDeIntrare().isEmpty() && worldClass.getLocuriOcupateSala().size() < capacitate) {


                                adaugare = worldClass.getCoadaDeIntrare().peek();
                                worldClass.getLocuriOcupateSala().add(adaugare);
                                worldClass.getCoadaDeIntrare().poll();
                                System.out.println("Am adaugat antrenorul " + cuContCreat1.getEmail() + " din coada de intrare la data si ora " + momentulIntrarii);

                            } else if (worldClass.getLocuriOcupateSala().size() < capacitate) {
                                worldClass.getLocuriOcupateSala().add(cuContCreat1);
                                System.out.println(cuContCreat1.getEmail() + " a intrat in sala la data si ora " + momentulIntrarii);

                            } else {
                                for (Object o : worldClass.getCoadaDeIntrare()) {
                                    if (o instanceof Antrenor) {
                                        Antrenor incaUnu = (Antrenor) o;
                                        if (incaUnu.equals(cuContCreat1)) {
                                            avemInCoada = true;
                                        }
                                    }
                                }
                                if (avemInCoada == false) {
                                    System.out.println("Am adaugat in coada");
                                    worldClass.getCoadaDeIntrare().add(cuContCreat1);
                                }
                            }

                        }


                        break;


                    case "IESE_DIN_SALA":

                        cuContCreat = null;
                        cuContCreat1 = null;
                        boolean prezentInSala = false;
                        for (Abonat i : listaAbonati) {

                            if (i.getEmail().equals(comanda[1])) {

                                cuContCreat = i;
                            }
                        }

                        for (Antrenor i : listaAntrenor) {
                            if (i.getEmail().equals(comanda[1])) {
                                cuContCreat1 = i;
                            }
                        }

                        for (Object o : worldClass.getLocuriOcupateSala()) {
                            if (o instanceof Abonat) {
                                Abonat abonat = (Abonat) o;
                                if (abonat.equals(cuContCreat)) {
                                    prezentInSala = true;

                                }
                            }
                        }
                        for (Object o : worldClass.getLocuriOcupateSala()) {
                            if (o instanceof Antrenor) {
                                Antrenor antrenorul = (Antrenor) o;
                                if (antrenorul.equals(cuContCreat1)) {
                                    prezentInSala = true;

                                }
                            }
                        }

                        if (prezentInSala == false && cuContCreat != null) {
                            bufferedWriter.write("Persoana cu adresa " + cuContCreat.getEmail() + " nu este in sala\n");
                            bufferedWriter.flush();
                        } else {
                            worldClass.getLocuriOcupateSala().remove(cuContCreat);
                        }


                        if (prezentInSala == false && cuContCreat1 != null) {
                            bufferedWriter.write("Persoana cu adresa " + cuContCreat1.getEmail() + " nu este in sala\n");
                            bufferedWriter.flush();
                        } else {
                            worldClass.getLocuriOcupateSala().remove(cuContCreat1);
                        }

                    case "VIZUALIZARE_PERSOANE_CU_ANTRENOR":

                        Map<Antrenor, List<Abonat>> vizualizareLista = new HashMap<>();


                        for (Antrenor i : listaAntrenor) {
                            if (!i.getListaDeCursanti().isEmpty()) {
                                vizualizareLista.put(i, i.getListaDeCursanti());
                            }
                        }

                        System.out.println(vizualizareLista);


                        break;

                    case "VIZUALIZARE_ABONATI":
                        TreeSet<Abonat> abonatiSortati = new TreeSet<>();


                        for (Abonat i : listaAbonati) {
                            abonatiSortati.add(i);

                        }

                        abonatiSortati.forEach(e -> System.out.println(e + " are progresul " + e.getProgress()));

                        break;

                    case "VIZUALIZARE_ANTRENORI":

                        TreeSet<Antrenor> antrenoriSortati = new TreeSet<>();


                        for (Antrenor i : listaAntrenor) {
                            antrenoriSortati.add(i);

                        }


                        antrenoriSortati.forEach(e -> System.out.println(e + " are " + e.getListaDeCursanti().size() + " cursanti"));

                        break;

//                    case "PERSISTA_ABONATI":
//
//                       String url = "jdbc:mysql://localhost:3306/examen";
//                        Connection connection = DriverManager.getConnection(url,"root","");
//                        Statement statement = connection.createStatement(   );
//                        String instertAbonati = "INSERT INTO abonati VALUES (1,'raoul.alexandru@playtika.com','Raoul','BetterNow31',6)";
////                        String deleteAbonati = "DELETE FROM abonati WHERE ID_Abonat =1";
//                        statement.executeUpdate(instertAbonati);
//                        bufferedWriter.write("Am adaugat abonatul in tabela\n");
//                        bufferedWriter.flush();
//                       break;
//
//                    case "PERSISTA_ANTRENORI":
//
//                        String url1 = "jdbc:mysql://localhost:3306/examen";
//                        Connection connection1= DriverManager.getConnection(url1,"root","");
//                        Statement statement1 = connection1.createStatement();
//                        String instertAntrenori = "INSERT INTO antrenori VALUES (1,'Alexandru.Peticila@telacad.ro','Alexandru','TelAcad_Examen44',10)";
////                        String deleteAntrenor = "DELETE FROM antrenori WHERE ID_Antrenor = 1";
//
//                        statement1.executeUpdate(instertAntrenori);
//                        bufferedWriter.write("Am adaugat antrenorii in tabela\n");
//                        bufferedWriter.flush();
//
//
//                        break;






                }


            }


//            worldClass.getObservers().forEach(e -> System.out.println(e));
//            listaAbonati.forEach(e -> System.out.println(e.getID() + " " + e.getEmail() + " " + e.getName() + " Face parte din abonati"));
//            listaAntrenor.forEach(e -> System.out.println(e.getID() + " " + e.getEmail() + " face parte din antrenori"));
//            worldClass.getLocuriOcupateSala().forEach(e -> System.out.println(e + " in sala"));
//            worldClass.getCoadaDeIntrare().forEach(e -> System.out.println(e + " In coada"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 //         catch (SQLException throwables) {
 //            throwables.printStackTrace();
//      }


    }


    private static boolean uniqueMail(List<Abonat> listaAbonati, List<Antrenor> listaAntrenor, boolean duplicat, BufferedWriter bufferedWriter, String[] comanda) throws IOException {
        for (Abonat i : listaAbonati) {
            if (i.getEmail().equals(comanda[1])) {


                bufferedWriter.write("Adresa de email " + i.getEmail() + " este deja utilizata!\n");
                bufferedWriter.flush();
                return false;
            }

        }
        for (Antrenor i : listaAntrenor) {
            if (i.getEmail().equals(comanda[1])) {

                bufferedWriter.write("Adresa de email " + i.getEmail() + " este deja utilizata!\n");
                bufferedWriter.flush();
                return false;
            }
        }
        return true;
    }


    private static boolean passwordVerification(BufferedWriter bufferedWriter, String[] comanda) throws IOException {
        if (!comanda[4].equals(comanda[5])) {

            bufferedWriter.write("Parole diferite, nu se poate face adaugarea!\n");
            bufferedWriter.flush();
            return false;
        }
        if (comanda[4].equals(comanda[5])) {
            String[] lungime = comanda[5].split("");
            if (lungime.length < 8) {
                bufferedWriter.write("Parola prea scurta\n");
                bufferedWriter.flush();
                return false;
            }

        }
        return true;
    }

    private static boolean emailVerification(int counter1, BufferedWriter bufferedWriter, String[] verifStructura) throws IOException {
        if (verifStructura.length == 2) {
            boolean avem = false;
            String[] punct = verifStructura[1].split("");
            for (String litera : punct) {

                counter1++;
                if (litera.equals(".")) {
                    avem = true;

                }

            }
            if (avem == true && counter1 > 1) {
//                System.out.println("Email valid");
                return true;
            } else {
                bufferedWriter.write("Adresa de email invalida\n");
                bufferedWriter.flush();
                return false;

            }

        } else {
            bufferedWriter.write("Adresa de email invalida\n");
            bufferedWriter.flush();
            return false;

        }

    }
}
