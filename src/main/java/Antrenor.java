import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Antrenor implements Observer, Comparable {


    private String email;
    private String name;
    private List<Date> intrari;
    private int numarMaxCursanti;
    private List<Abonat> listaDeCursanti;

    private String parola;
    private int ID;


    public Antrenor(String email, String name) {
        this.email = email;
        this.name = name;
        this.ID = Abonat.getNr();
        Abonat.setNr(Abonat.getNr());
        this.listaDeCursanti = new ArrayList<>(numarMaxCursanti);
        this.parola = null;
    }


    public List<Abonat> getListaDeCursanti() {
        return listaDeCursanti;
    }


    public String getEmail() {
        return email;
    }

    public String getParola() {
        return parola;
    }

    public String getName() {
        return name;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public void setNumarMaxCursanti(int numar) {
        numarMaxCursanti = numar;
    }

    public int getNumarMaxCursanti() {
        return numarMaxCursanti;
    }

    public int getID() {
        return ID;
    }

    @Override

    public boolean equals(Object o) {
        if (o == null || !(o instanceof Antrenor)) {
            return false;
        }
        Antrenor kappa = (Antrenor) o;
        if (this.getEmail().equals(kappa.getEmail()) && this.getName().equals(kappa.getName())) {
            return true;
        }
        return false;

    }

    @Override
    public void subscribe(Subject subject) {
        subject.addObserver(this);
    }

    @Override
    public void unsubscribe(Subject subject) {
        subject.removeObserver(this);
    }

    @Override
    public void update(String news) {
//        System.out.println("A fost notificata persoana cu adresa de email " + getEmail() + ", stirea cu mesajul " + news);
    }


    @Override
    public String toString() {
        return this.email;
    }

    @Override
    public int hashCode() {
        int result = 17;

        result += 31 * this.getEmail().hashCode();
        result += 31 * this.getName().hashCode();

        return result;
    }

    @Override
    public int compareTo(Object o) {
        Antrenor a = (Antrenor) o;
        if (this.getListaDeCursanti().size() > a.getListaDeCursanti().size()) {
            return 1;
        } else if (this.getListaDeCursanti().size() == a.getListaDeCursanti().size()) {
            int compar = this.getName().compareTo(a.getName());
            if (compar > 0) {
                return 1;
            } else {
                return -1;
            }
        } else return -1;
    }
}
